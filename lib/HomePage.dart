import 'package:NorseApp/Models/CityModel.dart';
import 'package:NorseApp/Models/houseModel.dart';
import 'package:NorseApp/Widget/widgets.dart';
import 'package:NorseApp/assets/Fonts.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<City> citis = new List();
  List<House> houses = new List();

  WidgetsNorse widgetsNorse = new WidgetsNorse();

  @override
  void initState() {
    citis = List.from(jsonDataCity).map((e) => City.fromMap(e)).toList();
    houses = List.from(jsonDataHouse).map((e) => House.fromJson(e)).toList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffF7F8FA),
        appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(35),
                  bottomRight: Radius.circular(35),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff6B7F99),
                    blurRadius: 10,
                    spreadRadius: 1,
                    offset: Offset(0, -5),
                  )
                ]),
            padding: EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.arrow_back_ios,
                      color: Color(0xff484451),
                    ),
                    Spacer(),
                    Icon(
                      Icons.menu,
                      color: Color(0xff484451),
                    ),
                  ],
                ),
                Text(
                  "Search",
                  style: TextStyle(
                    color: Color(0xff484451),
                    fontFamily: FontApp.opumMai,
                    fontSize: 28,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "location",
                      style: TextStyle(
                        color: Color(0xff989898),
                        fontFamily: FontApp.opumMai,
                        fontSize: 10,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: width * 0.9,
                      child: TextField(
                        decoration: InputDecoration(
                          fillColor: Color(0xffEFF0F8),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          prefixIcon: Icon(Icons.search),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 35),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("more"),
                      Icon(Icons.keyboard_arrow_down),
                    ],
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(width, height * 0.4),
        ),
        body: ListView(
          children: [
            Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Popular Cities",
                    style: TextStyle(
                      color: Color(0xff323643),
                      fontFamily: FontApp.opumMai,
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                  Text(
                    "SEE ALL",
                    style: TextStyle(
                      fontFamily: FontApp.opumMai,
                      color: Color(0xffADA8B8),
                      fontSize: 10.0,
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: width,
              height: height * 0.3,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: citis.length,
                  itemBuilder: (context, int index) {
                    return widgetsNorse.cityCards(citis[index].name,
                        citis[index].details, citis[index].image, context);
                  }),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Pppular Properties",
                    style: TextStyle(
                      fontFamily: FontApp.opumMai,
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "SEE ALL",
                    style: TextStyle(
                      fontFamily: FontApp.opumMai,
                      color: Color(0xffADA8B8),
                      fontSize: 10.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: width,
              height: height * 0.3,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: houses.length,
                  itemBuilder: (context, int index) {
                    return widgetsNorse.houseCards(
                        houses[index].price,
                        houses[index].bed,
                        houses[index].bath,
                        houses[index].image,
                        context);
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
