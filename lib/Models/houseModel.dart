class House {
  String price;
  String bed;
  String bath;
  String image;

  House();

  House.fromJson(Map<String, dynamic> json) {
    try {
      this.price = json['price'];
      this.bed = json['bed'];
      this.bath = json['bath'];
      this.image = json['image'];
    } catch (e) {
      print(e);
    }
  }
}

List<Map<String, dynamic>> jsonDataHouse = [
{
  "price":"360000",
  "bed":"3",
  "bath":"2",
  "image":"assets/imgs/house/house1.jpg",
},
{
  "price":"280000",
  "bed":"2",
  "bath":"1",
  "image":"assets/imgs/house/house2.jpg",
},
{
  "price":"1400000",
  "bed":"5",
  "bath":"3",
  "image":"assets/imgs/house/house3.jpg",
},
{
  "price":"550000",
  "bed":"3",
  "bath":"2",
  "image":"assets/imgs/house/house4.jpg",
},
{
  "price":"450000",
  "bed":"3",
  "bath":"2",
  "image":"assets/imgs/house/house5.jpg",
},
{
  "price":"2000000",
  "bed":"6",
  "bath":"4",
  "image":"assets/imgs/house/house6.jpg",
},

];
