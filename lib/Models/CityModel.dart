import 'package:flutter/cupertino.dart';

class City {
  String name;
  String details;
  String image;

  City();

  City.fromMap(Map<String, dynamic> json) {
    try {
      this.name = json['name'];
      this.details = json['details'];
      this.image = json['image'];
    } catch (e) {
      print(e);
    }
  }
}


  List<Map<String, dynamic>> jsonDataCity = [ 
    {
    "name": "Antalya",
    "details":"150 properties",
    "image":"assets/imgs/city/antalya.jpg",
    },    
    {
    "name": "Barcelona",
    "details":"139 properties",
    "image":"assets/imgs/city/barcelona.jpg",
    },   
     {
    "name": "Dubai",
    "details":"145 properties",
    "image":"assets/imgs/city/dubai.jpg",
    },  
      {
    "name": "Monaco",
    "details":"160 properties",
    "image":"assets/imgs/city/monaco.jpg",
    },   
     {
    "name": "Torento",
    "details":"120 properties",
    "image":"assets/imgs/city/torento.jpg",
    },

  ];