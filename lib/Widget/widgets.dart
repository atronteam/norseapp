import 'package:NorseApp/assets/Fonts.dart';
import 'package:flutter/material.dart';

class WidgetsNorse {
  Widget cityCards(
      String name, String details, String imageCity, BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      width: width * 0.42,
      height: height * 0.25,
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.white,
              blurRadius: 15,
              spreadRadius: 5,
            )
          ]),
      child: Stack(
        fit: StackFit.loose,
        children: [
          Positioned(
              child: Container(
            width: width * 0.45,
            height: height * 0.28,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: Image.asset(
                imageCity,
                fit: BoxFit.cover,
              ),
            ),
          )),
          Positioned(
              child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20), bottom: Radius.circular(20)),
                gradient: LinearGradient(colors: [
                  Colors.transparent,
                  Color(0xff323643),
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
          )),
          Positioned(
              bottom: 15,
              left: 5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: FontApp.opumMai,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    details,
                    style: TextStyle(
                      color: Colors.blue,
                      fontFamily: FontApp.opumMai,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  Widget houseCards(String price, String bed, String bath, String image,
      BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Stack(
      fit: StackFit.loose,
      children: [
        Align(
          alignment: Alignment.topCenter,
          child: Container(
            margin: EdgeInsets.all(7),
            width: width * 0.33,
            height: height * 0.2,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              child: Image.asset(
                image,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Positioned(
            left: 7,
            bottom: 5,
            width: width * 0.2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "\u0024 ${price.substring(0, 3)},${price.substring(3)}",
                  style: TextStyle(
                    color: Color(0xff484451),
                    fontFamily: FontApp.opumMai,
                    fontSize: 12,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "$bed bed",
                      style: TextStyle(
                          color: Color(0xff589FF8),
                          fontFamily: FontApp.opumMai,
                          fontSize: 12),
                    ),
                    Text(
                      " $bath bath",
                      style: TextStyle(
                          color: Color(0xff589FF8),
                          fontFamily: FontApp.opumMai,
                          fontSize: 12),
                    ),
                  ],
                )
              ],
            ))
      ],
    );
  }
}
